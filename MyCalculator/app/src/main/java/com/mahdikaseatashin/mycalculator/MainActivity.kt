package com.mahdikaseatashin.mycalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var lastNum = false
    var lastDot = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClick(view: View) {
        when (view.tag) {
            "digit" -> {
                lastNum = true
                tvResult.append((view as Button).text)
            }
            "result" -> {
                calculateResult()
                tvResult.text = removeZero(tvResult.text.toString())
                lastNum = true
                lastDot = true
            }
            "operator" -> {
                println(lastNum)
                if (lastNum && !isOperatorAdded(tvResult.text.toString())) {
                    tvResult.append((view as TextView).text)
                    lastNum = false
                    lastDot = false
                }

            }
            "clear" -> {
                if (tvResult.length() > 1) {
                    tvResult.text = tvResult.text.substring(0, tvResult.length() - 1)
                    when (tvResult.text[(tvResult.text.lastIndex)]) {
                        '.' -> {
                            lastDot = true
                            lastNum = false
                        }
                        '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' -> {
                            lastNum = true
                        }
                        else -> {
                            lastNum = false
                        }
                    }
                } else if (tvResult.length() == 1) {
                    tvResult.text = ""
                    lastNum = false
                    lastDot = false
                }
            }
            "reset" -> {
                lastNum = false
                lastDot = false
                tvResult.text = ""
            }
            "dot" -> {
                if (!lastDot && lastNum) {
                    tvResult.append(".")
                    lastNum = false
                    lastDot = true
                }

            }
        }
    }

    private fun calculateResult() {
        var value = tvResult.text
        var prefix = ""
        try {
            if (tvResult.text.startsWith("-")) {
                prefix = "-"
                value = value.substring(1)
            }
            if (value.contains("-")) {
                val split = value.split("-")
                var firstNum = split[0]
                val secondNum = split[1]
                if (prefix.isNotEmpty()) {
                    firstNum = prefix + firstNum
                }
                if (secondNum.isNotEmpty())
                    tvResult.text = (firstNum.toDouble() - secondNum.toDouble()).toString()
            } else if (value.contains("+")) {
                val split = value.split("+")
                var firstNum = split[0]
                val secondNum = split[1]
                if (tvResult.text.startsWith("-"))
                    firstNum = prefix + firstNum
                if (secondNum.isNotEmpty())
                    tvResult.text = (firstNum.toDouble() + secondNum.toDouble()).toString()
            } else if (value.contains("*")) {
                val split = value.split("*")
                var firstNum = split[0]
                val secondNum = split[1]
                if (tvResult.text.startsWith("-"))
                    firstNum = prefix + firstNum
                if (secondNum.isNotEmpty())
                    tvResult.text = (firstNum.toDouble() * secondNum.toDouble()).toString()
            } else if (value.contains("/")) {
                val split = value.split("/")
                var firstNum = split[0]
                val secondNum = split[1]
                if (tvResult.text.startsWith("-"))
                    firstNum = prefix + firstNum
                if (secondNum.isNotEmpty())
                    tvResult.text = (firstNum.toDouble() / secondNum.toDouble()).toString()
            } else if (value.contains("/")) {
                val split = value.split("/")
                var firstNum = split[0]
                val secondNum = split[1]
                if (tvResult.text.startsWith("-"))
                    firstNum = prefix + firstNum
                if (secondNum.isNotEmpty())
                    tvResult.text = (firstNum.toDouble() / secondNum.toDouble()).toString()
            }
        } catch (e: ArithmeticException) {
            e.printStackTrace()
        }
    }

    private fun removeZero(result: String): String {
        var value = result
        if (result.contains(".0"))
            value = result.substring(0,result.length-2)
        return value
    }

    private fun isOperatorAdded(value: String): Boolean {
        return if (value.startsWith("-")) {
            false
        } else {
            value.contains("/") || value.contains("*") || value.contains("+") || value.contains("-")
        }

    }

}
